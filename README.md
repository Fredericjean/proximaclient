
# PROXIMA PLAYER by @Novelab


## Installation

pré-requis : Windows 10 64 bits

Télécharger NODEJS et l’installer
https://nodejs.org/en/

Télécharger la dernière version de firefox NIGHTLY 64 BITS
https://www.mozilla.org/en-US/firefox/nightly/all/

configurer firefox nightly pour openvr  https://webvr.rocks/firefox
PAS BESOIN DE Configurer FIREFOX NIGHTLY
SUPPORT DE OCULUS DANS LE BUILD !!!!! :)

télécharger l’application

lancer dans le dossier
node app

avec Firefox Nightly ouvrir http://localhost:3000/

## Configuration
### Video
La configuration des séquences se fait dans data/config.json

la vidéo principale est "mainvideo":"medias/laval.mov",
(rappel : la video principal ne contient pas les sons qui sont gérés en externe )

### Sons
Pour Les sons, voir dans le index.html
ligne 55
preloadQueue=[
  {id:"ambisonique", src:"medias/lueurs_ambix_v3.mp4"},
  {id:"ambianceSound", src:"medias/lueurs_2d_v2.mp3"},
    {id:"ambiance_top", src:"medias/lueurs_punisher_top.mp3"},
      {id:"ambiance_bottom", src:"medias/lueurs_punisher_bottom.mp3"},
	      {id:"config", src:"data/config.json"}
];
